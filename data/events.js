const mongoCollections = require("../config/mongoCollections");
const eventsCollection = mongoCollections.events;
const uuid = require("uuid");
const validator = require("../utilities/validate");

const exportedMethods =
{
    // @Description: Returns a array of all event objects
    //
    // @Return: {Array} events
    //
    async GetAllEvents() 
    {
        const events = await eventsCollection();
        return await events.find({}).toArray();
    },

    // @Description: Returns a collection of event objects based on location
    //
    // @Param: location {string}
    //
    // @Return: {Array} events
    //
    async GetEventsByLocation(location) 
    {
        try
        {
            validator("location", "string", location, null, null);

            const events = await eventsCollection();

            return await events.find({ location: location }).toArray();
        }
        catch(e)
        {
            throw e;
        }  
    },

    // @Description: Returns a single event given the event id
    //
    // @Param: id {string}
    //
    // @Return: {Object} event
    //
    async GetEventById(id) 
    {
        try
        {
            validator("id", "string", id, null, null);

            const events = await eventsCollection();

            const event = await events.findOne({ _id: id });

            if (!event) 
            {
                throw "Event not found";
            }

            return event;
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Creates a new event
    //
    // @Param: name {string}
    // @Param: location {string}
    // @Param: dateTime {DateTime}
    // @Param: maxPlayers {Integer}
    // @Param: playersPerTeam {Integer}
    // @Param: organizingUserId {string} Primary key for user who made this event
    //
    // @Return: {Object} event
    //
    async AddEvent(name, location, dateTime, maxPlayers, playersPerTeam, organizingUserId) 
    {
        try
        {
            validator("name", "string", name, null, null);
            validator("location", "string", location, null, null);
            validator("dateTime", "object", dateTime, null, null);
            validator("maxPlayers", "number", maxPlayers, 0, 500);
            validator("playersPerTeam", "number", playersPerTeam, 0, 30);
            validator("organizingUserId", "string", organizingUserId, null, null);

            const events = await eventsCollection();

            // Setup the new event object to be inserted to the collection
            let newEvent =
            {
                _id: uuid.v4(),
                name: name,
                location: location,
                dateTime: dateTime,
                maxPlayers: maxPlayers,
                playersPerTeam: playersPerTeam,
                eventOrganizer: organizingUserId,
                players: [],
                teams: []
            };
    
            // Insert the event into the collection and get the ID of that inserted item
            const insertInfo = await events.insertOne(newEvent);
    
            if (insertInfo.insertedCount == 0) throw "Could not add new event";
    
            // Return the added event
            return await this.GetEventById(insertInfo.insertedId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Removes an event
    //
    // @Param: id {string}
    //
    // @Return: None
    //
    async RemoveEvent(id) 
    {
        try
        {
            validator("id", "string", id, null, null);

            const events = await eventsCollection();

            const deletionInfo = await events.removeOne({ _id: id });
    
            if (deletionInfo.deletedCount === 0) 
            {
                throw `Could not delete event with id of ${id}`;
            }
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Updates an event
    //
    // @Param: id {string}
    // @Param: updatedEvent {Object} Object containing the event fields that are to be updated
    //
    // @Return: {Object} event
    //
    async UpdateEvent(id, updatedEvent) 
    {
        try
        {
            validator("id", "string", id, null, null);
            validator("updatedEvent", "object", updatedEvent, null, null);
    
            const events = await eventsCollection();
    
            let updatedEventData = {};
    
            // Set up the fields to be updated
            // The update object should only include fields that need to change
            if (updatedEvent.name) { updatedEventData.name = updatedEvent.name; }
            if (updatedEvent.location) { updatedEventData.location = updatedEvent.location; }
            if (updatedEvent.dateTime) { updatedEventData.dateTime = updatedEvent.dateTime; }
            if (updatedEvent.maxPlayers) { updatedEventData.maxPlayers = updatedEvent.maxPlayers; }
            if (updatedEvent.playersPerTeam) { updatedEventData.playersPerTeam = updatedEvent.playersPerTeam; }
            if (updatedEvent.teams) { updatedEventData.teams = updatedEvent.teams; }
    
            let updateCommand =
            {
                $set: updatedEventData
            };
    
            // Execute update
            await events.updateOne({ _id: id }, updateCommand)
    
            // Return the updated Event
            return await this.GetEventById(id);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Adds a specific user to a specific event
    //
    // @Param: userId {string}
    // @Param: eventId {string}
    //
    // @Return: {Object} event
    //
    async AddUserToEvent(userId, eventId) 
    {
        try
        {
            validator("userId", "string", userId, null, null);
            validator("eventId", "string", eventId, null, null);
    
            const events = await eventsCollection();
    
            let updateCommand =
            {
                $addToSet:
                {
                    players: userId
                }
            }
    
            await events.updateOne({ _id: eventId }, updateCommand);
    
            // Return the updated Event
            return await this.GetEventById(eventId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Removes a specific user from a specific event
    //
    // @Param: userId {string}
    // @Param: eventId {string}
    //
    // @Return: {Object} event
    //
    async RemoveUserFromEvent(userId, eventId) 
    {
        try
        {
            validator("userId", "string", userId, null, null);
            validator("eventId", "string", eventId, null, null);
    
            const events = await eventsCollection();
    
            let updateCommand =
            {
                $pull:
                {
                    players: userId
                }
            }
    
            await events.updateOne({ _id: eventId }, updateCommand);
    
            // Return the updated Event
            return await this.GetEventById(eventId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Algorithm for building out the teams for the event
    //
    // @Param: eventId {string}
    //
    // @Return: {Integer} Id of updated event
    //
    async BuildTeamsForEvent(eventId) 
    {
        try
        {
            validator("eventId", "string", eventId, null, null);

            let event = await this.GetEventById(eventId);
            let playersOnEvent = event.players;
            const playersPerTeam = event.playersPerTeam;
            const teamsToBuild = Math.ceil(playersOnEvent.length / playersPerTeam);
            let teams = [];
    
            // Shuffle players
            playersOnEvent = this.Shuffle(playersOnEvent);
    
            // Create teams
            for (var team = 1; team <= teamsToBuild; team++) 
            {
                let newTeam =
                {
                    _id: team,
                    players: []
                }
    
                teams.push(newTeam);
            }
    
            // Put each player on a team
            var teamToPlacePlayerOn = 0;
            for (var player = 0; player < playersOnEvent.length; player++) 
            {
                if (teams[teamToPlacePlayerOn].players.length == playersPerTeam) 
                {
                    teamToPlacePlayerOn++;
                }
    
                let team = teams[teamToPlacePlayerOn];
                let playerId = playersOnEvent[player];
    
                team.players.push(playerId);
            }
    
            // Update event with populated teams
            let updatedEventData =
            {
                teams: teams
            }
    
            // Update and return event
            return await this.UpdateEvent(eventId, updatedEventData);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Randomly shuffles an array
    //
    // @Param: array {Array}
    //
    // @Return: {Array} Shuffled array
    //
    Shuffle(array) 
    {
        try
        {
            validator("array", "array", array, null, null);

            var currentIndex = array.length;
            var temporaryValue;
            var randomIndex;
    
            while (0 !== currentIndex) 
            {
                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
    
                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
    
            return array;
        }
        catch (e)
        {
            throw e;
        }
    }
};

module.exports = exportedMethods;