const mongoCollections = require("../config/mongoCollections");
const usersCollection = mongoCollections.users;
const uuid = require("uuid");
const bcrypt = require('bcrypt-nodejs');
const validator = require("../utilities/validate");

const exportedMethods =
{
    // @Description: Returns a array of all user objects
    //
    // @Return: {Array} Array of users
    //
    async GetAllUsers() 
    {
        const users = await usersCollection();
        return await users.find({}).toArray();
    },

    // @Description: Returns a user based on email
    //
    // @Param: emailAddress {string}
    //
    // @Return: {Object} user
    //
    async GetUserByEmail(emailAddress) 
    {
        try
        {
            validator("emailAddress", "string", emailAddress, null, null);

            const users = await usersCollection();

            const user = await users.findOne({ email: emailAddress });
    
            return user;
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Returns a single user given the user id
    //
    // @Param: id {string}
    //
    // @Return: {Object} user
    //
    async GetUserById(id) 
    {
        try
        {
            validator("id", "string", id, null, null);

            const users = await usersCollection();
    
            const user = await users.findOne({ _id: id })
    
            if (!user) 
            {
                throw "User not found";
            }
    
            return user;
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Creates a new user
    //
    // @Param: firstName {string}
    // @Param: lastName {string}
    // @Param: emailAddress {string}
    // @Param: password {string} Pre-hashed
    // @Param: city {string}
    // @Param: state {string}
    // @Param: picture {string} - URL
    // @Param: skillLevel {string} - enumeration
    // @Param: privilege {string} - enumeration
    //
    // @Return: {Object} user
    //
    async AddUser(firstName, lastName, emailAddress, password, city, state, picture, skillLevel, privilege) 
    {
        try
        {
            validator("firstName", "string", firstName, null, null);
            validator("lastName", "string", lastName, null, null);
            validator("emailAddress", "string", emailAddress, null, null);
            validator("password", "string", password, null, null);
            validator("city", "string", city, null, null);
            validator("state", "string", state, null, null);
            validator("picture", "string", picture, null, null);
            validator("skillLevel", "string", skillLevel, null, null);
            validator("privilege", "string", privilege, null, null);
    
            const users = await usersCollection();
    
            // Hash the password
            const hashedPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    
            // Setup the new event object to be inserted to the collection
            let newUser =
            {
                _id: uuid.v4(),
                firstName: firstName,
                lastName: lastName,
                email: emailAddress,
                hashedPassword: hashedPassword,
                city: city,
                state: state,
                picture: picture,
                skillLevel: skillLevel,
                privilege: privilege,
                badLogin: -1,
                lockUntil: -1,
                events: []
            };
    
            // Insert the event into the collection and get the ID of that inserted item
            const insertInfo = await users.insertOne(newUser);
    
            if (insertInfo.insertedCount == 0) throw "Could not add new user";
    
            // Return the added event
            return await this.GetUserById(insertInfo.insertedId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Removes a user
    //
    // @Param: id {string}
    //
    // @Return: None
    //
    async RemoveUser(id) 
    {
        try
        {
            validator("id", "string", id, null, null);

            const users = await usersCollection();
    
            const deletionInfo = await users.removeOne({ _id: id });
    
            if (deletionInfo.deletedCount === 0) 
            {
                throw `Could not delete user with id of ${id}`;
            }
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Updates a user
    //
    // @Param: id {string}
    // @Param: updatedUser {Object} Object containing the user fields that are to be updated
    //
    // @Return: {Object} user
    //
    async UpdateUser(id, updatedUser) 
    {
        try
        {
            validator("id", "string", id, null, null);
            validator("updateUser", "object", updatedUser, null, null);
    
            const users = await usersCollection();
    
            let updatedUserData = {};
    
            // Set up the fields to be updated
            // The update object should only include fields that need to change
            if (updatedUser.firstName) { updatedUserData.firstName = updatedUser.firstName; }
            if (updatedUser.lastName) { updatedUserData.lastName = updatedUser.lastName; }
            if (updatedUser.email) { updatedUserData.email = updatedUser.email; }
            if (updatedUser.hashedPassword) { updatedUserData.hashedPassword = updatedUser.hashedPassword; }
            if (updatedUser.city) { updatedUserData.city = updatedUser.city; }
            if (updatedUser.state) { updatedUserData.state = updatedUser.state; }
            if (updatedUser.picture) { updatedUserData.picture = updatedUser.picture; }
            if (updatedUser.skillLevel) { updatedUserData.skillLevel = updatedUser.skillLevel; }
            if (updatedUser.privilege) { updatedUserData.privilege = updatedUser.privilege; }
            if (updatedUser.badLogin) { updatedUserData.badLogin = updatedUser.badLogin; }
            if (updatedUser.lockUntil) { updatedUserData.lockUntil = updatedUser.lockUntil; }
    
            let updateCommand =
            {
                $set: updatedUserData
            };
    
            // Execute update
            await users.updateOne({ _id: id }, updateCommand)
    
            // Return the updated Event
            return await this.GetUserById(id);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Adds a specific event to a specific user
    //
    // @Param: eventId {string}
    // @Param: userId {string}
    //
    // @Return: {Object} user
    //
    async AddEventToUser(eventId, userId) 
    {
        try
        {
            validator("eventId", "string", eventId, null, null);
            validator("userId", "string", userId, null, null);
    
            const users = await usersCollection();
    
            let updateCommand =
            {
                $addToSet:
                {
                    events: eventId
                }
            }
    
            await users.updateOne({ _id: userId }, updateCommand);
    
            // Return the updated user
            return await this.GetUserById(userId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Removes a specific event from a specific user
    //
    // @Param: eventId {string}
    // @Param: userId {string}
    //
    // @Return: {Object} user
    //
    async RemoveEventFromUser(eventId, userId) 
    {
        try
        {
            validator("eventId", "string", eventId, null, null);
            validator("userId", "string", userId, null, null);
    
            const users = await usersCollection();
    
            let updateCommand =
            {
                $pull:
                {
                    events: eventId
                }
            }
    
            await users.updateOne({ _id: userId }, updateCommand);
    
            // Return the updated user
            return await this.GetUserById(userId);
        }
        catch (e)
        {
            throw e;
        }
    },

    // @Description: Validates user login information. Populates errorData object
    // for information to display on the GUI
    //
    // @Param: email {String}
    // @Param: password {String} Pre-hashed password
    // @Param: errorData {Object} 
    //
    // @SideEffects: If a user enters an invalid password then a counter for the bad login
    // attempts is incremented. If this counter reaches 5 then a variable lockUntil is set
    // with a dateTime object used to mark a time when the account will be unlocked.
    //
    // @Return: {Boolean} Valid = true / Invalid = false
    //
    async IsValidUserData(email, password, errorData) 
    {
        try
        {
            validator("email", "string", email, null, null);
            validator("password", "string", password, null, null);
            validator("errorData", "object", errorData, null, null);
    
            try 
            {
                if (!email) { errorData.message = "No email provided."; errorData.badEmail = true; return false; };
                if (!password) { errorData.message = "No password provided."; errorData.badPassword = true; return false; };
    
                const users = await usersCollection();
    
                const user = await users.findOne({ email: email });
    
                if (!user) 
                {
                    errorData.message = `No user exists with the email ${email}`;
                    errorData.badEmail = true;
    
                    return false;
                }
    
                // If this user exists make sure its not locked out currently
                if (user.lockUntil != -1) 
                {
                    var now = new Date();
                    if (now.getTime() > user.lockUntil.getTime()) 
                    {
                        // unlock
                        user.lockUntil = -1;
                        user.badLogin = -1;
    
                        await this.UpdateUser(user._id, { badLogin: user.badLogin, lockUntil: user.lockUntil });
                    }
                    else 
                    {
                        // locked still
                        errorData.message = `Too many login attempts. This account is temporarily locked!`;
                        errorData.badAll = true;
    
                        return false;
                    }
                }
    
                if (!bcrypt.compareSync(password, user.hashedPassword)) 
                {
                    errorData.message = `Invalid user information provided.`;
                    errorData.badAll = true;
    
                    // Add a bad login attempt to this user
                    updatedUser = {};
                    updatedUser.badLogin = (user.badLogin == -1 ? 1 : user.badLogin + 1);
    
                    // If bad logins == 5 then set a lock time now() + 1 hour
                    if (updatedUser.badLogin == 5) 
                    {
                        var now = new Date();
                        now.setTime(now.getTime() + (1 * 60 * 60 * 1000))
    
                        updatedUser.lockUntil = now;
                    }
    
                    // Update user
                    await this.UpdateUser(user._id, updatedUser);
    
                    return false;
                }
                else 
                {
                    // If this user has accrued bad login attempts but finally logs in safely 
                    // then clear bad logins
                    if (user.badLogin > 0) 
                    {
                        await this.UpdateUser(user._id, { badLogin: -1 });
                    }
                }
    
                return true;
            }
            catch (e) 
            {
                errorData.message = e.message;
                errorData.badAll = true;
                
                return false;
            }
        }
        catch (e)
        {
            throw e;
        }
    },
};

module.exports = exportedMethods;
