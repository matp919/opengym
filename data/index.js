const userData = require('./users');
const eventsData = require('./events');

module.exports = 
{
    users: userData,
    events: eventsData
};