const data = require('./data');
const usersAPI = data.users;
const eventsAPI = data.events;

const exportedMethods =
{
    async SeedData() 
    {
        let users = [];
        let events = [];

        users.push(await usersAPI.AddUser("Matt", "Perrelli", "matp919@gmail.com", "password", "Clifton", "New Jersey", "", "Advanced", "User"));
        users.push(await usersAPI.AddUser("John", "Doe", "jdoe@site.com", "password2", "Wayne", "New Jersey", "", "Beginner", "User"));
        users.push(await usersAPI.AddUser("Greg", "Blue", "gb@hotmail.com", "password3", "Paterson", "New Jersey", "", "Intermediate", "User"));
        users.push(await usersAPI.AddUser("Rob", "Stark", "rob@gmail.com", "password4", "Jersey City", "New Jersey", "", "Beginner", "User"));
        users.push(await usersAPI.AddUser("Paige", "Doe", "pdoe@site.com", "password5", "Newark", "New Jersey", "", "Advanced", "User"));
        users.push(await usersAPI.AddUser("Alyssa", "Willow", "lyssw@site.com", "password6", "Clifton", "New Jersey", "", "Intermediate", "User"));
        users.push(await usersAPI.AddUser("Ramsey", "Bolton", "rbol@site.com", "password6", "Lodi", "New Jersey", "", "Advanced", "User"));
        users.push(await usersAPI.AddUser("Ryan", "Wrynn", "rwynn@gmail.com", "password6", "Garfield", "New Jersey", "", "Advanced", "User"));
        users.push(await usersAPI.AddUser("John", "Madden", "jmadden@nfl.com", "password6", "Paterson", "New Jersey", "", "Intermediate", "User"));
        users.push(await usersAPI.AddUser("Seth", "Rollins", "curbstomp2016@site.com", "password6", "East Orange", "New Jersey", "", "Beginner", "User"));
        users.push(await usersAPI.AddUser("Tormund", "Giantsbane", "Giantsbane@gmail.com", "password6", "Ridgewood", "New Jersey", "", "Beginner", "User"));
        users.push(await usersAPI.AddUser("Saquon", "Barkley", "gaints2018@hotmail.com", "password6", "Hoboken", "New Jersey", "", "Beginner", "User"));
        users.push(await usersAPI.AddUser("Aly", "Willow", "lyssw@site.com", "password6", "Wayne", "New Jersey", "", "Intermediate", "User"));

        events.push(await eventsAPI.AddEvent("Event 1", "East Hanover", new Date("2019-01-05T16:00:00Z"), 24, 6, users[0]._id));
        events.push(await eventsAPI.AddEvent("Event 2", "Hoboken", new Date("2019-01-15T16:30:00Z"), 12, 6, users[0]._id));
        events.push(await eventsAPI.AddEvent("Event 3", "Clifton", new Date("2019-01-15T18:00Z"), 24, 6, users[0]._id));
        events.push(await eventsAPI.AddEvent("Event 4", "Hoboken", new Date("2019-01-20T19:00:00Z"), 12, 6, users[0]._id));

        for (i = 1; i < users.length; i++) 
        {
            for (j = 0; j < events.length; j++) 
            {
                await eventsAPI.AddUserToEvent(users[i]._id, events[j]._id);
                await usersAPI.AddEventToUser(events[j]._id, users[i]._id);
            }
        }

        for (j = 0; j < events.length; j++) 
        {
            await eventsAPI.BuildTeamsForEvent(events[j]._id)
        }
    }
}

module.exports = exportedMethods;

exportedMethods.SeedData();

return 1;