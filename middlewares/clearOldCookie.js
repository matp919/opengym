module.exports = function (req, res, next) 
{
    if (req.cookies.AuthCookie && !req.session.user) 
    {
        res.clearCookie('AuthCookie');
    }

    next();
}