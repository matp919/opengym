const settings = require("../config/settings.json");
const appConstants = settings.appConstants;

module.exports = function (req, res, next) 
{
    // Attach user data if we have it
    if (req.session.user && req.cookies.AuthCookie) 
    {
        res.locals.user = req.session.user;
        delete res.locals.user.hashedPassword;
    }

    // Attach application constants
    res.locals.title = appConstants.title;

    next();
}