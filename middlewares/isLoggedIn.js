module.exports = function(req, res, next)
{
    if (req.session.user && req.cookies.AuthCookie) 
    {
        next();
    } 
    else 
    {
        res.redirect('/login');
    }   
}