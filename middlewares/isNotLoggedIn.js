module.exports = function(req, res, next)
{
    if (req.session.user && req.cookies.AuthCookie) 
    {
        res.redirect('/');
    } 
    else 
    {
        next();
    }   
}