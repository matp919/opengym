module.exports = function(argName, expectedType, argument, min, max)
{
    if(expectedType == "array" && !Array.isArray(argument))
    {
        throw argName + " must be an array.";
    }

    if(expectedType != "array" && typeof argument != expectedType && argument != null)
    {
        throw argName + " parameter must be a " + expectedType + ".";
    }
    
    if(!isNaN(argument))
    {
        if(argument < min || (max != null ? argument > max : false))
        {
            if(max != null)
                msg = argName + " parameter must be within the bounds " + min + " - " + max;
            else
                msg = argName + " parameter must be greater than " + min;
    
            throw msg;
        }
    }  
}