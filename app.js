const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser")
const configRoutes = require("./routes");
const exphbs = require("express-handlebars");
const session = require('express-session');
const clearCookie = require('./middlewares/clearOldCookie');
const attachDataToResponse = require('./middlewares/attachDataToResponse');
const hbs = require('handlebars');

const app = express();

// Set path to public data and client side code
const static = express.static(__dirname + "/public");
app.use("/public", static);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// Express session
app.use(session(
{
    key: 'AuthCookie',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: 
    {
        expires: 600000
    }
}));

// Middleware to clear old cookie
app.use(clearCookie);

// If a user is logged in then attach that user data to be rendered
app.use(attachDataToResponse);

// Set view engine details
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
hbs.registerHelper('select', function(selected, options) {
    return options.fn(this).replace(
        new RegExp(' value=\"' + selected + '\"'),
        '$& selected="selected"');
});
app.set("view engine", "handlebars");

// Set routes
configRoutes(app);

app.listen(3000, () => 
{
    console.log("We've now got a server!");
    console.log("Your routes will be running on http://localhost:3000");
});