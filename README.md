# OpenGym

OpenGym is a online application for organizing local open gym events. The application utilizes NodeJS for server-side logic and manageing a REST API via Express.js. HTML/CSS/Javascript are used to develop the front end. Users are able to create an account, login, signup for events and view other players and what they are saigned up for. The application will organize players into teams within events.

## Installation

This is a [Node.js](https://nodejs.org/) module.

### Prerequisites

To run locally, ensure that Node JS and MongoDB are installed.

### Running Locally

After cloning, open the command line and navigate to the root directory of the project. Then run

```
npm install
```

This will install the dependencies. To start the local version of the project, run

```
npm run-script seed
```

This will seed the database with default data

```
npm start
```

Once you see, "Your routes will be running on http://localhost:3000", then it means everything is ready. Navigate to that URL to begin using the project.

## Built With/Dependencies

- [bcrypt-nodejs](http://ghub.io/bcrypt-nodejs): A native JS bcrypt library for NodeJS.
- [body-parser](http://ghub.io/body-parser): Node.js body parsing middleware
- [cookie-parser](http://ghub.io/cookie-parser): cookie parsing with signatures
- [express](http://ghub.io/express): Fast, unopinionated, minimalist web framework
- [express-handlebars](http://ghub.io/express-handlebars): A Handlebars view engine for Express which doesn&#39;t suck.
- [express-session](http://ghub.io/express-session): Simple session middleware for Express
- [handlebars](http://ghub.io/hbs): Express.js template engine plugin for Handlebars
- [mongodb](http://ghub.io/mongodb): The official MongoDB driver for Node.js
- [uuid](http://ghub.io/uuid): RFC4122 (v1, v4, and v5) UUIDs

## Authors

* **Matthew Perrelli**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details