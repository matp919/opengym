const userRoutes = require('./user');
const loginRoutes = require('./login');
const signupRoutes = require('./signup');
const eventRoutes = require('./event');
const isLoggedIn = require('../middlewares/isLoggedIn');

const constructorMethod = (app) => 
{
    app.use("/users", userRoutes);
    app.use("/login", loginRoutes);
    app.use("/signup", signupRoutes);
    app.use("/events", eventRoutes);

    app.get("/logout", isLoggedIn, async (req, res) =>
    {
        res.clearCookie('AuthCookie');
        res.redirect('/');
    });

    app.get("/", async (req, res) => 
    {
        res.render("index");
    });

    app.use("*", async (req, res) => 
    {
        res.status(404).render("error", { error: "Page Not Found!" })
    });
};

module.exports = constructorMethod;