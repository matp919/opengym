const express = require('express');
const router = express.Router();
const data = require('../data');
const usersAPI = data.users;
const eventsAPI = data.events;
const bcrypt = require('bcrypt-nodejs');
const isLoggedIn = require('../middlewares/isLoggedIn');

router.get("/", async (req, res) => 
{
    try 
    {
        const users = await usersAPI.GetAllUsers()

        res.render("users/viewAll", { users });
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

router.get("/view/:id", async (req, res) => 
{
    try 
    {
        const viewUser = await usersAPI.GetUserById(req.params.id);
        var enableEditButton = false;

        // If on the profile page we have a logged in user looking at their own
        // profile we need to enable the edit button
        if(req.session.user)
        {
            if(req.session.user._id == req.params.id)
            {
                enableEditButton = true;
            }
        } 

        // Replace eventID's with some event Data
        for(i = 0; i < viewUser.events.length; i++)
        {
            let event = await eventsAPI.GetEventById(viewUser.events[i]);
            viewUser.events[i] = event;
        }

        res.render("users/view", { viewUser, enableEditButton });
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

router.get("/edit", isLoggedIn, async (req, res) => 
{
    try 
    {
        const userId = req.session.user._id;
        const editUser = await usersAPI.GetUserById(userId);
        res.render("users/edit", { editUser });
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

router.post("/edit", isLoggedIn, async (req, res) => 
{
    try 
    {
        let errorData = {};
        const userData = await usersAPI.GetUserById(req.session.user._id);
        const formData = req.body;
        let userDataToUpdate = {};

        if(formData.firstName != userData.firstName) { userDataToUpdate.firstName = formData.firstName };
        if(formData.lastName != userData.lastName) { userDataToUpdate.lastName = formData.lastName };
        if(formData.city != userData.city) { userDataToUpdate.city = formData.city };
        if(formData.skillLevel != userData.skillLevel) { userDataToUpdate.skillLevel = formData.skillLevel };
        
        // Handle email
        if(formData.email != userData.email)
        {
            // Check if email has been used by another.
            let user = await usersAPI.GetUserByEmail(formData.email);
            if(!errorData.message && user)
            {
                errorData.message = "This email address is already in use.";
                errorData.badEmail = true;
            }
            else
            {
                userDataToUpdate.email = formData.email;
            }
        }
        
        // Handle new password
        if(formData.password != "")
        {
            // Ensure passwords equal
            if(formData.password != formData.passwordRetype)
            {
                errorData.message = "The entered passwords do not match.";
                errorData.badPassword = true;
            }
            else
            {
                const hashedPassword = bcrypt.hashSync(formData.password, bcrypt.genSaltSync(8), null);
                userDataToUpdate.hashedPassword = hashedPassword;
            }
        }
        
        if(!errorData.message)
        {
            if(Object.keys(userDataToUpdate).length > 0)
            {
                const updatedUser = await usersAPI.UpdateUser(userData._id, userDataToUpdate);
            }
            
            res.redirect("view/" + userData._id);
        }
        else
        {
            const editUser = formData;
            res.render('users/edit', { editUser, errorData });
        }   
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

module.exports = router;