const express = require('express');
const router = express.Router();
const data = require('../data');
const eventsAPI = data.events;
const usersAPI = data.users;
const isLoggedIn = require('../middlewares/isLoggedIn');

router.get("/", async (req, res) => 
{
    try 
    {
        let events = await eventsAPI.GetAllEvents()

        for(i = 0; i < events.length; i++)
        {
            events[i].dateTime = events[i].dateTime.toLocaleString();
            events[i].playerCount = events[i].players.length;
        }

        res.render("events/viewAll", { events });
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

router.get("/view/:id", async (req, res) => 
{
    try 
    {
        const viewEvent = await eventsAPI.GetEventById(req.params.id);
        var userAttendingEvent = false;

        // Replace reference to attending players with the actual user objects
        for (i = 0; i < viewEvent.players.length; i++) 
        { 
            let player = await usersAPI.GetUserById(viewEvent.players[i]);
            viewEvent.players[i] = player;

            if(req.session.user)
            {
                if(req.session.user._id == player._id)
                {
                    userAttendingEvent = true;
                }
            }
        }

        // Replace references to users on each team with the actual user objcts
        for(i = 0; i < viewEvent.teams.length; i++)
        {
            const team = viewEvent.teams[i];
            for(j = 0; j < team.players.length; j++)
            {
                let player = await usersAPI.GetUserById(team.players[j]);
                team.players[j] = player;
            }
        }

        // Replace date object with string
        viewEvent.dateTime = viewEvent.dateTime.toLocaleString();
        viewEvent.playerCount = viewEvent.players.length;

        res.render("events/view", { viewEvent, userAttendingEvent });
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

router.post("/view/:id", isLoggedIn, async (req, res) => 
{
    try 
    {
        const attend = (req.body.attending == "true");

        if(attend)
        {
            await eventsAPI.AddUserToEvent(req.session.user._id, req.params.id);
            await usersAPI.AddEventToUser(req.params.id, req.session.user._id);
        }
        else
        {
            await eventsAPI.RemoveUserFromEvent(req.session.user._id, req.params.id);
            await usersAPI.RemoveEventFromUser(req.params.id, req.session.user._id);
        }

        await eventsAPI.BuildTeamsForEvent(req.params.id);

        res.redirect("/events/view/" + req.params.id);
    } 
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

module.exports = router;