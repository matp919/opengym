const express = require('express');
const router = express.Router();
const data = require('../data');
const usersAPI = data.users;
const isNotLoggedIn = require('../middlewares/isNotLoggedIn');

router.get("/", isNotLoggedIn, async (req, res) => 
{
    res.render("login");
});

router.post("/", async (req, res) => 
{
    var email = req.body.email;
    var password = req.body.password;
    var errorData = {};

    try 
    {
        const validUser = await usersAPI.IsValidUserData(email, password, errorData);

        if (!validUser) 
        {
            res.status(500).json({ errorData });
        }
        else 
        {
            req.session.user = await usersAPI.GetUserByEmail(email);
            res.redirect('/');
        }
    }
    catch (e) 
    {
        res.status(500).render("error", { error: e.message })
    }
});

module.exports = router;