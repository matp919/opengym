const express = require('express');
const router = express.Router();
const data = require('../data');
const usersAPI = data.users;
const isNotLoggedIn = require('../middlewares/isNotLoggedIn');

router.get("/", isNotLoggedIn, async (req, res) => 
{
    res.render("signup");
});

router.post("/", async (req, res) => 
{
    const userData = req.body;
    var errorData = {};

    try 
    {
        const { 
            firstName, 
            lastName, 
            email, 
            password, 
            passwordRetype,
            city, 
            skillLevel } = userData;

        // Check if passwords are equal
        if(password != passwordRetype)
        {
            errorData.message = "The entered passwords do not match.";
            errorData.badPassword = true;
        }

        // Check if email has been used by another.
        const user = await usersAPI.GetUserByEmail(email);
        if(!errorData.message && user)
        {
            errorData.message = "This email address is already in use.";
            errorData.badEmail = true;
        }

        if(!errorData.message)
        {
            const newUser = await usersAPI.AddUser(
                firstName, lastName, email, password, city, "New Jersey", "", skillLevel, "User");
    
            res.redirect('/users/view/' + newUser._id);
        }
        else
        {
            res.render('signup', { errorData, userData });
        }
    } 
    catch(e) 
    {
        res.status(500).json({ error: e.message });
    }
});

module.exports = router;